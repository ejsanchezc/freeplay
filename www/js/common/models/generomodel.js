angular.module('entidadmodulo',[])

.factory('entiedadgenero',function(){
 function entiedadgenero(pid,pname,ppicture){
     this.id=pid;
     this.name=pname;
     this.picture=ppicture;     
 }
 entiedadgenero.build=function(data){
     if(!data)
     return null;     
     return new entiedadgenero(data.id,data.name,data.picture);
 }
 entiedadgenero.prototype.toJson=function(){
     return angular.toJson(this);
 }

entiedadgenero.fromJsonBunch=function(data){
    if (angular.isArray(data)){
         return data.map(entiedadgenero.build).filter(Boolean);
    }
    return entiedadgenero.build(data);
}

return entiedadgenero;
})

.factory('entiedadartistas',function(){
 function entiedadartistas(pid,pname,ppicture_big){
     this.id=pid;
     this.name=pname;
     this.picture_big=ppicture_big;     
 }
 entiedadartistas.build=function(data){
     if(!data)
     return null;     
     return new entiedadartistas(data.id,data.name,data.picture_big);
    
 }

 entiedadartistas.prototype.toJson=function(){
     return angular.toJson(this);
 }

entiedadartistas.fromJsonBunch=function(data){
    if (angular.isArray(data)){
         return data.map(entiedadartistas.build).filter(Boolean);
    }
    return entiedadartistas.build(data);
}

return entiedadartistas;


})

.factory('entiedadlista',function(){
 function entiedadlista(pid,ptitle,ppreview,partist,palbum){
     this.id=pid;
     this.title=ptitle; 
     this.preview=ppreview;
     this.artist=partist;
     this.album=palbum;
 }
 entiedadlista.build=function(data){
     if(!data)
     return null;     
      //console.log("Select",data.id,data.title);
     return new entiedadlista(data.id,data.title,data.preview,data.artist,data.album);
    
    
 }

 entiedadlista.prototype.toJson=function(){
     return angular.toJson(this);
 }

entiedadlista.fromJsonBunch=function(data){
    if (angular.isArray(data)){
         return data.map(entiedadlista.build).filter(Boolean);
    }
    return entiedadlista.build(data);
}

return entiedadlista;


})

.factory('entidadmusica',function(){
 function entidadmusica(pid,ptitle,ppreview,partist,palbum){
     this.id=pid;
     this.title=ptitle; 
     this.preview=ppreview;
     this.artist=partist;
     this.album=palbum;
 }
 entidadmusica.build=function(data){
     if(!data)
     return null;     
      //console.log("Select",data.id,data.title);
     return new entidadmusica(data.id,data.title,data.preview,data.artist,data.album);
    
    
 }

 entidadmusica.prototype.toJson=function(){
     return angular.toJson(this);
 }

entidadmusica.fromJsonBunch=function(data){
    if (angular.isArray(data)){
         return data.map(entidadmusica.build).filter(Boolean);
    }
    return entidadmusica.build(data);
}

return entidadmusica;


})