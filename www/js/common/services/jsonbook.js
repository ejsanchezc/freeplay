angular.module('jsonbookmodulo', ['entidadmodulo'])

    .factory('generoservicio', function ($http, $q, entiedadgenero,entiedadartistas,entiedadlista,entidadmusica) {
        var generoservicio = {};

        generoservicio.generos = [];
        generoservicio.artistas = [];
        generoservicio.artistaseleccionado = [];
        generoservicio.musicas = [];
        generoservicio.obtenergeneros = function () {
            var deferred = $q.defer();
            $http.get('http://api.deezer.com/genre').then(
                function (response) {
                    generoservicio.generos = entiedadgenero.fromJsonBunch(response.data.data);
                    deferred.resolve(generoservicio.generos);
                },
                function (error) {
                    deferred.reject();
                }
            );
            return deferred.promise;

        };
          generoservicio.obtenerlista = function (id) {
            var deferred = $q.defer();
            
            $http.get('http://api.deezer.com/artist/'+id+'/top').then(
                
                function (response) {
                    generoservicio.artistaseleccionado = entiedadlista.fromJsonBunch(response.data.data);
                    console.log("LISTAASSS",generoservicio.artistaseleccionado);
                    deferred.resolve(generoservicio.artistaseleccionado);
                    

                },
                function (error) {
                    deferred.reject();
                }
            );


            return deferred.promise;

        };


        generoservicio.obtenerartistas = function (id) {
            var deferred = $q.defer();
         
            $http.get('http://api.deezer.com/genre/'+id+'/artists').then(
                function (response) {
                    generoservicio.artistas = entiedadartistas.fromJsonBunch(response.data.data);
                    deferred.resolve(generoservicio.artistas);
                   // console.log("artistas del genero",generoservicio.artistas);
                },
                function (error) {
                    deferred.reject();
                }
            );
            return deferred.promise;

        };

        generoservicio.obtenerbuscador = function (musica) {
            var deferred = $q.defer();
            console.log(musica);
            $http.get('http://api.deezer.com/search?q='+musica).then(
                function (response) {
                    generoservicio.musicas = entidadmusica.fromJsonBunch(response.data.data);
                    
                    
                    deferred.resolve(generoservicio.musicas);
                   
                    
                },
                function (error) {
                    deferred.reject();
                }
            );
            return deferred.promise;

        };

    

        return generoservicio;
    })