angular.module('musicamodule')
  .controller('menucontroller', function ($rootScope, $scope, firebase, $timeout, $firebaseAuth, $state) {
    $rootScope.style = "#3C6DF5";
    $rootScope.xddddxdd = "bar-positive";
    $rootScope.changeColor = function (color) {
      console.log("cambiando color");
      if (color === 1) {
        console.log("cambiando color rojo");
        $rootScope.xddddxdd = "bar-assertive";
        $rootScope.style = "#F44336";
      } else if (color === 2) {
        console.log("cambiando color azul");
        $rootScope.xddddxdd = "bar-positive";
        $rootScope.style = "#3C6DF5";
      } else {
        console.log("cambiando color orange");
        $rootScope.xddddxdd = "bar-energized";
        $rootScope.style = "#FF9800";
      }
    };
    $scope.logout = function () {
      $rootScope.nombre = "";
      $rootScope.email = "";
      $rootScope.photo = "";
      $rootScope.provier = "";
      $rootScope.usuariologeado = "";
      console.log("usuario en el menu: ", $rootScope.usuariologeado);
      $firebaseAuth().$signOut();
      $state.go('login');
    };
  });