angular.module('starter')
    .controller('LoginCtrl', function ($location, $scope, $rootScope, $state, $ionicLoading, $ionicPopup, firebase, $timeout, $firebaseAuth) {
        $rootScope.getprofile = function () {
            $scope.response = $firebaseAuth().$getAuth();
            $rootScope.usuariologeado = $scope.response.providerData;
            angular.forEach($rootScope.usuariologeado, function (value, key) {
                console.log("usuario datos: ", value);
                $rootScope.nombre = value.displayName;
                $rootScope.email = value.email;
                $rootScope.photo = value.photoURL;
                $rootScope.provier = value.providerId;
            });
            console.log("user actuallll : ", $rootScope.usuariologeado.length);
            console.log("user nombre : ", $scope.nombre);
            console.log("actual user: array:  ", $rootScope.usuariologeado);
        }

        $scope.doRegister = function (name, email, password) {
            $ionicLoading.show({
                content: 'Cargando',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
            $timeout(function () {
                if (name !== undefined && email !== undefined && password !== undefined) {
                    $firebaseAuth().$createUserWithEmailAndPassword(email, password).then(function (user) {
                        $ionicLoading.hide();
                        user = firebase.auth().currentUser;
                        console.log(name);
                        console.log(user);
                        logUser(name, user);
                        alert('Gracias por Registrarse. Porfavor inicie sesión', $ionicPopup);
                        $state.go('login');
                    }, function (error) {
                        console.log(error.code);
                        console.log(error.message);
                        switch (error.code) {
                            case "auth/weak-password":
                                alert('Ingrese una contraseña mayor a 6 digitos', $ionicPopup);
                                $ionicLoading.hide();
                                break;
                            case "auth/email-already-in-use":
                                alert('El email ya esta registrado', $ionicPopup);
                                break;
                            case "operation-not-allowed":
                                alert('', $ionicPopup);
                            default:
                                alert('Error de Servidor', $ionicPopup);
                                break;
                        }
                        $ionicLoading.hide();
                    });
                } else {
                    $ionicLoading.hide();
                    alert('Porfavor ingrese sus datos', $ionicPopup);
                    $state.go('register');
                }
            }, 1000);
        };

        $scope.doLogin = function (email, password) {
            $ionicLoading.show({
                content: 'Cargando',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
            $timeout(function () {
                if (email !== undefined && password !== undefined) {
                    $firebaseAuth().$signInWithEmailAndPassword(email, password).then(function (user) {
                        $ionicLoading.hide();
                        $rootScope.user = user;
                        console.log("Usuario autenticado: ", $rootScope.user);
                        //$state.go('app.genero');
                    }).catch(function (error) {
                        //console.log(error);
                        //Excepciones 
                        //console.log(error.code);
                        switch (error.code) {
                            case "auth/argument-error":
                                alert('Porfavor ingrese sus datos', $ionicPopup);
                                break;
                            case "auth/invalid-email":
                                alert('Formato de e-mail Invalido', $ionicPopup);
                                break;
                            case "auth/user-not-found":
                                alert('Su cuenta no esta registrada', $ionicPopup);
                                break;
                            case "auth/network-request-failed":
                                alert('Hubo problemas de conexión. Intente nuevamente', $ionicPopup);
                                break;
                            case "auth/user-disabled":
                                alert('Su cuenta esta deshabilitada', $ionicPopup);
                                break;
                            default:
                                alert('email y/o contraseña incorrectas', $ionicPopup);
                                break;
                        }
                        $ionicLoading.hide();
                        $state.go('login');
                    });
                } else {
                    $ionicLoading.hide();
                    alert('Porfavor ingrese sus datos', $ionicPopup);
                }
            }, 1500);
        };
        $rootScope.usuarrioLog = "mi user";
        $scope.facebook = function () {
            $ionicLoading.show({
                content: 'Cargando',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
            var provider = new firebase.auth.FacebookAuthProvider();
            console.log("ENTRAND A LOGEARME CON FB", provider);
            $firebaseAuth().$signInWithRedirect(provider).then(function (result) {
                firebase.auth().getRedirectResult().then(function (result) {
                    $rootScope.usuarrioLog = result.user;
                    //$rootScope.nombre = usuarrioLog.displayName;
                    /*$rootScope.email = usuarrioLog.email;
                    $rootScope.photo = usuarrioLog.photoURL;*/
                    console.log("user: redirect", $rootScope.usuarrioLog);
                    $rootScope.getprofile();
                    $ionicLoading.hide();
                    $state.go('app.genero');
                }).catch(function (error) {
                    $ionicLoading.hide();
                    console.log("error");
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    alert('Error de autenticacion, por favor revise su conexion a internet.' + error);
                });
            }).catch(function (error) {
                $ionicLoading.hide();
                //Excepciones 
                //console.log(error.code);
                switch (error.code) {
                    case "auth/argument-error":
                        alert('Porfavor ingrese sus datos', $ionicPopup);
                        break;
                    case "auth/invalid-email":
                        alert('Formato de e-mail Invalido', $ionicPopup);
                        break;
                    case "auth/user-not-found":
                        alert('Su cuenta no esta registrada', $ionicPopup);
                        break;
                    case "auth/network-request-failed":
                        alert('Hubo problemas de conexión. Intente nuevamente', $ionicPopup);
                        break;
                    case "auth/user-disabled":
                        alert('Su cuenta esta deshabilitada', $ionicPopup);
                        break;
                    default:
                        alert('email y/o contraseña incorrectas', error);
                        break;
                }
                $ionicLoading.hide();
                $state.go('login');
            });

            console.log("user autenticado: ", $rootScope.usuarrioLog);
        };

        $scope.logout = function () {
            $firebaseAuth().$signOut();
        };

        $scope.google = function () {
            var provider = new firebase.auth.GoogleAuthProvider();
            console.log("ENTRAND A LOGEARME CON GOOGLE", provider);
            $firebaseAuth().$signInWithRedirect(provider).then(function (result) {
                firebase.auth().getRedirectResult().then(function (result) {
                    var token = result.credential.accessToken;
                    var user = result.user;
                    console.log("user: redirect", user);
                    $rootScope.getprofile();
                    $state.go('app.genero');
                }).catch(function (error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                });
            }).catch(function (error) {
                console.log("error: ", error);
            });
        };

        $scope.options = {
            autoplay: 5000,
            loop: false,
            effect: 'fade',
            speed: 900,


        }

        $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
            // data.slider is the instance of Swiper
            $scope.slider = data.slider;
        });

        $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
            console.log('Slide change is beginning');
        });

        $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
            // note: the indexes are 0-based
            $scope.activeIndex = data.slider.activeIndex;
            $scope.previousIndex = data.slider.previousIndex;
        });

    });