angular.module('musicamodule')

    .controller('listacontroller', function ($scope, $rootScope, $state, generoservicio, $cordovaMedia) {
        console.log("DESDE LISTA", $rootScope.id);
        if ($rootScope.artista !== null || $rootScope.artista !== undefined) {
            $scope.img = $rootScope.artista.picture_big;
            $scope.artista = $rootScope.artista.name;
            //  $rootScope.objArtista;
            id = $rootScope.artista.id;
            console.log("id del artista", $rootScope.artista.id);
            generoservicio.obtenerlista(id).then(function (response) {

                $scope.listaEncontrado = response;
                console.log("Encontro las musica", $scope.listaEncontrado);

            }).catch(function (error) {
                alert('Artista no encontrado');
                console.log(error);
            });

        } else {
            $state.go('app.artistas');
        }

        $scope.reproducirAleatorio = function () {
            $rootScope.listaAleatoria = $scope.listaEncontrado;
            console.log("Lista aleatoria: ", $rootScope.listaAleatoria);
            $state.go('app.reproductor');
        };

        $scope.getMusica = function (idMusic) {
            console.log("lista de canciones: ", idMusic);
            //  $location.url('#/app/lista'+artista.id);
            $rootScope.idMusic = idMusic;
            $rootScope.artista;
            if ($rootScope.media === undefined) {
                $rootScope.media = $cordovaMedia.newMedia($rootScope.idMusic.preview);
                $rootScope.lista = lista;

                /*
                if($rootScope.media === undefined){
                    //if($rootScope.media.getDuration()==$rootScopemedia.getCurrentPosition())
                    $rootScope.media = $cordovaMedia.newMedia($rootScope.lista.preview);
                    console.log("create media primera vez", $rootScope.media);
                }else{
                    $rootScope.media.stop();
                    console.log("musica stop");
                    $rootScope.media.media.src = $rootScope.idMusic.preview;
                    console.log("cambiando el src", $rootScope.media);
                }
                //console.log("Info lista",$rootScope.lista,$rootScope.artista);*/
                $state.go('app.reproductor');

            }
        };

        $scope.download = function (musica) {
            window.open(musica.preview, '_system', 'location=no,clearsessioncache=no,clearcache=no');
        };


    })
    .directive('fileDownload', function () {
        return {
            restrict: 'E', // applied on 'element'
            scope: {
                fileurl: '@fileurl',
                linktext: '@linktext'
            },
            template: '<a style="color: white;" href="{{ fileurl }}" target="_system" download>{{ linktext }}</a>', // need this so that the inner HTML can be re-used
            link: function (scope, elem, attrs) {
                /* Ref: https://thinkster.io/egghead/isolate-scope-at
                 * This block is used when we have
                 * scope: {
                     fileurl: '=fileurl',
                     linktext: '=linktext'     
                   }          
                 scope.fileurl = attrs.fileurl;
                 scope.linktext = attrs.linktext;*/
            }
        }
    });