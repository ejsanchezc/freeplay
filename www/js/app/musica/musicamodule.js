angular.module('musicamodule', ['entidadmodulo', 'jsonbookmodulo'])
    .config(function ($stateProvider) {
        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'js/app/menu/menu.html',
                controller: 'menucontroller'
            })

            .state('app.profile', {
                url: '/profile',
                views: {
                    'menuContent': {
                        templateUrl: 'js/app/musica/profile/profile.html',
                        controller: 'profileController'
                    }
                }
            })
            
            .state('app.genero', {
                url: '/genero',
                views: {
                    'menuContent': {
                        templateUrl: 'js/app/musica/generos/genero.html',
                        controller: 'generocontroller',
                        resolve: {
                            generos: function (generoservicio) {
                                return generoservicio.obtenergeneros();
                            }
                        }
                    }
                }
            })
            .state('app.reproductor', {
                url: '/reproductor',
                views: {
                    'menuContent': {
                        templateUrl: 'js/app/musica/reproductor/reproductor.html',
                        controller: 'reproductorcontroller'

                    }
                }
            })
            .state('app.artistas', {
                url: '/artistas',
                views: {
                    'menuContent': {
                        templateUrl: 'js/app/musica/artistas/artistas.html',
                        controller: 'artistascontroller'
                        //resolve: {                            
                        //  artistas: function (generoservicio) {
                        //console.log("HOLA DE MODULO", generoservicio.obtenerartistas());
                        //    return generoservicio.obtenerartistas();
                        //}                            
                        //}
                    }
                }
            })
            .state('app.buscador', {
                url: '/buscador',
                views: {
                    'menuContent': {
                        templateUrl: 'js/app/musica/buscador/buscador.html',
                        controller: 'buscadorcontroller',
                        
                       
                    }
                }
            })
            .state('app.reprobuscador', {
                url: '/reprobuscador',
                views: {
                    'menuContent': {
                        templateUrl: 'js/app/musica/reprobuscador/reprobuscador.html',
                        controller: 'reprocontroller',
                        
                       
                    }
                }
            })
            .state('app.lista', {
                url: '/lista',
                views: {
                    'menuContent': {
                        templateUrl: 'js/app/musica/lista/lista.html',
                        controller: 'listacontroller'

                    }
                }
            })
            .state('app.localmusic', {
                url: '/localmusic',
                views: {
                    'menuContent': {
                        templateUrl: 'js/app/musica/localmusic/localmusic.html',
                        controller: 'LocalMusicCtrl'

                    }
                }
            })

    })