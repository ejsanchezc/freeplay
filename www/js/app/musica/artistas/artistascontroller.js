angular.module('musicamodule')
  .controller('artistascontroller', function ($scope, $rootScope, $state, generoservicio, $location, $ionicLoading, $timeout) {
    $ionicLoading.show({
      duration: 1500,
      noBackdrop: true,
      template: '<p class="item-icon-left">Cargando...<ion-spinner icon="lines"/></p>'
    });

    if ($rootScope.genero !== null || $rootScope.genero !== undefined) {

      //  $rootScope.objArtista;
      id = $rootScope.genero.id;
      $scope.genero = $rootScope.genero.name;
      console.log("DESDE LISTA artista", id);
      generoservicio.obtenerartistas(id).then(function (response) {

        $scope.artistas = response;
        //console.log("Encontro",$scope.listaEncontrado);

      }).catch(function (error) {
        alert('Artista no encontrado');
        console.log(error);
      });

    } else {
      $state.go('app.genero');
    }


    $scope.getAlbum = function (artista) {
      //console.log("hOLA ARTISTA",id);
      //  $location.url('#/app/lista'+artista.id);
      $rootScope.artista = artista;
      $state.go('app.lista');

    }

  });