angular.module('musicamodule')

    .controller('reproductorcontroller', function ($scope, $rootScope, $state, $cordovaMedia) {
        $rootScope.numeroReproducir = 1;
        $scope.duracion = function (media) {
            // Get duration
            var counter = 0;
            var timerDur = null;
            $rootScope.totalDuracion = 0;
            timerDur = setInterval(function () {
                counter = counter + 100;
                if (counter > 2000) {
                    clearInterval(timerDur);
                }
                var dur = media.getDuration();
                if (dur > 0) {
                    clearInterval(timerDur);
                    $rootScope.totalDuracion = dur;
                    console.log("duracion: root ", $rootScope.totalDuracion);
                }
            }, 100);
        };

        $scope.currenPosition = function (media) {
            $rootScope.PosicionActual = 0;
            var mediaTimer = null;
            mediaTimer = setInterval(function () {
                // get media position
                media.getCurrentPosition(
                    // success callback
                    function (position) {
                        if (position > -1) {
                            //document.getElementById('audio_position').innerHTML = (position / 1000) + " sec";
                            $rootScope.PosicionActual = position;
                            console.log("positcion:  ", $rootScope.PosicionActual);
                            console.log("duracion: ", $rootScope.totalDuracion);
                            if ($rootScope.totalDuracion > 0) {
                                if ($rootScope.PosicionActual <= $rootScope.totalDuracion && $rootScope.PosicionActual > 0) {
                                    console.log("AUN SIGUE REPRODUCIENDOSE...");
                                } else {
                                    clearInterval(mediaTimer);
                                    console.log("REPRODUCIENDO LA SIGUIENTE CANCION");
                                    $scope.imprimir();
                                }
                            }
                        }
                    },
                    // error callback
                    function (e) {
                        console.log("Error getting pos=" + e);
                    }
                );
            }, 1000);
        };

        $scope.reproducir = function (musica) {
            $rootScope.artista = musica.artist.name;
            $rootScope.img = musica.album.cover_big;
            $rootScope.cancion = musica.title;
            console.log("MUSICAAAAAAAAAAAAAA: ", musica);
            console.log("repoduciendo .. : ", musica);
            $scope.pause = "pause";
            $scope.x = 0;
            if ($rootScope.actual === undefined) {
                $rootScope.actual = new Media(musica.preview);
                console.log("create media primera vez", $rootScope.actual);
                $rootScope.actual.play();
                $scope.duracion($rootScope.actual);
                $scope.currenPosition($rootScope.actual);
            } else {
                $rootScope.actual.stop();
                console.log("musica stop");
                console.log("Musica siguiente: ", musica);
                console.log("root media actual: ", $rootScope.actual);
                console.log("cambiando el src", $rootScope.actual);
                $rootScope.actual.src = musica.preview;
                $scope.changeAssets(musica);
                $rootScope.actual.play();
                $scope.duracion($rootScope.actual);
                $scope.currenPosition($rootScope.actual);
            }
        };

        $scope.changeAssets = function (musica) {
            $rootScope.artista = musica.artist.name;
            $rootScope.img = musica.album.cover_big;
            $rootScope.cancion = musica.title;
        }

        console.log("lista aleatoria en controller repro: ", $rootScope.listaAleatoria);
        var list = $rootScope.listaAleatoria;

        $scope.imprimir = function () {
            if ($rootScope.numeroReproducir >= 5) {
                $rootScope.numeroReproducir = 1;
            }
            //$scope.num = Math.floor(Math.random() * (5 - 1 + 1)) + 1;
            var contador = 0;
            console.log("aleatorio: ", $rootScope.numeroReproducir);
            $scope.rep = 0;
            console.log("imprimiendo");
            angular.forEach($rootScope.listaAleatoria, function (value, key) {
                //console.log("lista item", value);
                contador++;
                if ($scope.rep === 1) {
                    console.log("musica aun reproduciendo: canciones siguientes", value);
                } else {
                    if (contador === $rootScope.numeroReproducir) {
                        console.log("NUMERO DE CAMCION REPRODUCIENDO AHORA ES : *****************+ : ", $rootScope.numeroReproducir);
                        $rootScope.numeroReproducir = $rootScope.numeroReproducir + 1;
                        console.log("Reproduciendo, ", value);
                        $scope.reproducir(value);
                        $scope.rep = 1;
                    }
                }
            });
        };
        $scope.imprimir();


        /*if ($rootScope.lista !== null || $rootScope.lista !== undefined) {
            $scope.img = $rootScope.artista.picture_big;
            $scope.artista = $rootScope.artista.name;
        if ($rootScope.lista !== null || $rootScope.lista !== undefined) {
            $scope.img = $rootScope.lista.album;
            $scope.artista = $rootScope.lista.artist;
            $scope.cancion = $rootScope.lista.title;
            var src = $rootScope.lista.preview;
            console.log("lista aleatoria en controller repro: ", $rootScope.listaAleatoria);
            //$rootScope.media = $cordovaMedia.newMedia(src); //crea la music
            /*
            $rootScope.media.play();
            $scope.pause = "pause";
            $scope.i = 0;
            //$scope.duraciones = "Duracion";

            //DURACION
            var duration = function () {
                $rootScope.media.getDuration().then(
                    function (response) {
                        console.log("Mostrar Response",response);
                    });
            };
           // duration();

            $scope.duracion = $rootScope.media.getDuration();

*/
        $scope.iconopp = "pause";
        $scope.i = 1;
        $scope.playMedia = function () {
            console.log("haciendo clic al paly media");
            if ($scope.i === 1) {
                $rootScope.actual.pause();
                $scope.iconopp = "play";
                $scope.i = 0;
            } else {
                $rootScope.actual.play();
                $scope.iconopp = "pause";
                $scope.i = 1;
            }
        }
        /*
            //si getcurrentpisition === getduration
            //stop()
            //$rootmdeia = new src.
            //play.
        } else {
            $state.go('app.artistas');
        }*/
    });